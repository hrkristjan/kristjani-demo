export class Stock {
    symbol: string;
    name: string;
    price: number; 
    currentValue: number; 
    changePercent: number; 
    changeValue: number
  }