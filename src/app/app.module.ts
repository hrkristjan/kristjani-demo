import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Import custom modules
import { HttpClientModule } from '@angular/common/http';
import { DatePipe, DecimalPipe } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";

//Import custom service
import { ApiService } from './services/api/api.service';

//Import custom pages
import { HomeComponent } from './pages/home/home.component';

//Import custom components
import { StocksComponent } from './components/stocks/stocks.component';
import { StocksmodelComponent } from './components/stocksmodel/stocksmodel.component';
import { TabComponent } from './components/tabs/tab.component';
import { TabsComponent } from './components/tabs/tabs.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StocksComponent,
    StocksmodelComponent,
    TabComponent,
    TabsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [
    ApiService, 
    DatePipe,
    DecimalPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
