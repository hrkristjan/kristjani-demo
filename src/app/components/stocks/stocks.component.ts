
import { Component, OnInit } from '@angular/core';
//Import custom modules
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
//Import my services
import { ApiService } from '../../services/api/api.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  stocks: Object = [{ symbol : "", name : "", price: 0, currentValue: 0, changePercent: 0, changeValue: 0 }];

  date: Date = new Date();
  myDate: string = "";

  constructor(
    private api: ApiService,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {
    //I use Angulars Date Pipe to format the date
    this.myDate = this.datePipe.transform(this.date, 'EEEE, MMMM d, y');
  }

  ngOnInit() {
    //Start loading spinner
    this.spinner.show();

    //On component load I get the base data from the API service
    this.api.getStockData()
    .then(result => { //Success
      this.stocks = result;
      // Spinner ends after 1.5 seconds otherwise in some cases it disappears too quicky to understand what was on the screen
      setTimeout(() => {
        this.spinner.hide();
      }, 1500);      
    })
    .catch(error => { // Error
      //If there is an error, the spinner is hidden immediately
      this.spinner.hide();
      //Alert the user and log the error
      console.error(error);
      alert("An error occurred. Please try again later");
    }); 

  }

  addDay(date){
    //First I format the date to add 1 day
    var dt = new Date(date); 
    var newDate = new Date().setDate(dt.getDate()+1);
    this.myDate = this.datePipe.transform(newDate, 'EEEE, MMMM d, y');

    //Generate random number to either add 10% to stock price or subtract 10% from stock price
    var plusOrMinus = Math.random() < 0.5 ? 0.9 : 1.1;

    //Calculate the new values based on random number result
    for (const [key] of Object.entries(this.stocks)) {
      //If no current value is present, I use the initial stock price
      if(!this.stocks[key].currentValue){
        this.stocks[key].currentValue = this.stocks[key].price;
      }
      //all results are rounded to 2 decimal points at the front end
      this.stocks[key].currentValue = this.stocks[key].currentValue * plusOrMinus;
      this.stocks[key].changePercent = ((this.stocks[key].currentValue - this.stocks[key].price ) / this.stocks[key].price) * 100;
      this.stocks[key].changeValue = this.stocks[key].currentValue - this.stocks[key].price;
    }

  }

  resetData(){
    //Reset to todays value
    this.myDate = this.datePipe.transform(this.date, 'EEEE, MMMM d, y');
    //Start loading spinner
    this.spinner.show();
    //Get the values from 
    this.api.getStockData()
      .then(result => { //Success
        this.stocks = result;
        // Hide spinner immediately
        this.spinner.hide();
      })
      .catch(error => { // Error
        //Hide spinner
        this.spinner.hide();
        //Alert the user and log the error
        console.error(error);
        alert("An error occurred. Please try again later");
      }); 
  }

}
