import { Component, OnInit } from '@angular/core';
//Import custom modules
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
//Import my services
import { ApiService } from '../../services/api/api.service';
//Import custom models
import { Stock } from './../../models/stock.model';

@Component({
  selector: 'app-stocksmodel',
  templateUrl: './stocksmodel.component.html',
  styleUrls: ['./stocksmodel.component.css']
})
export class StocksmodelComponent implements OnInit {

  stocks: Stock[];
  date: Date = new Date();
  myDate: string = "";

  constructor(
    private api: ApiService,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {
    //I use Angulars Date Pipe to format the date
    this.myDate = this.datePipe.transform(this.date, 'EEEE, MMMM d, y');
  }

  ngOnInit() {
    this.spinner.show();
    this.api.getStockDataToModel().subscribe(
      result => {
      this.spinner.hide();
      this.stocks = result;
      }),
      error => {
        console.error('HTTP Error', error);
        alert("An error occurred. Please try again later");
      };
  }

  addDay(date){
    //First I format the date to add 1 day
    var dt = new Date(date); 
    var newDate = new Date().setDate(dt.getDate()+1);
    this.myDate = this.datePipe.transform(newDate, 'EEEE, MMMM d, y');

    //Generate random number to either add 10% to stock price or subtract 10% from stock price
    var plusOrMinus = Math.random() < 0.5 ? 0.9 : 1.1;

    //Calculate the new values based on random number result
    for (const [key] of Object.entries(this.stocks)) {
      //If no current value is present, I use the initial stock price
      if(!this.stocks[key].currentValue){
        this.stocks[key].currentValue = this.stocks[key].price;
      }
      //all results are rounded to 2 decimal points at the front end
      this.stocks[key].currentValue = this.stocks[key].currentValue * plusOrMinus;
      this.stocks[key].changePercent = ((this.stocks[key].currentValue - this.stocks[key].price ) / this.stocks[key].price) * 100;
      this.stocks[key].changeValue = this.stocks[key].currentValue - this.stocks[key].price;
    }

  }

  resetData(){
    //When reset is pressed, the date is changed to today and a new API request called.
    this.spinner.show();
    this.myDate = this.datePipe.transform(this.date, 'EEEE, MMMM d, y');
    this.api.getStockDataToModel().subscribe(result => {
      this.spinner.hide();
      this.stocks = result;
    }),
    error => {
      console.error('HTTP Error', error);
      alert("An error occurred. Please try again later");
    };
  }

}
