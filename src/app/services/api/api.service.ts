import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//import custom 
import {Observable} from 'rxjs'

//import custom models
import { Stock } from './../../models/stock.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = "https://staging-api.brainbase.com/stocks.php";

  constructor(private http: HttpClient) {}

  getStockData(){
    //Here I get the simple API request and return to stocks component
    return new Promise((resolve, reject) => {
      this.http.get(this.apiURL)
      .toPromise()
      .then(
        result => { // Success
          resolve(result);
        },
        error => { // Error
          reject(error);
        }
      );
    });   
  }

  //Here I get the simple API request and return to stocks model
  getStockDataToModel(): Observable<Stock[]> {
    return this.http.get<Stock[]>(this.apiURL);
  }

}
